const images = document.querySelectorAll('.image-to-show');
const firstImage = images[0];
const lastImage = images[images.length - 1];
const stopButton = document.querySelector('.btnStop');
const resumeButton = document.querySelector('.btnResume');

const slider = () => {
  const currentImage = document.querySelector('.visible');
  if(currentImage !== lastImage){
    currentImage.classList.remove('visible');
    currentImage.nextElementSibling.classList.add('visible'); 
  } else {
    currentImage.classList.remove('visible');
    firstImage.classList.add('visible'); 
  }
}

let timer = setInterval(slider, 3000);

stopButton.addEventListener('click', () => {
  clearInterval(timer);
  resumeButton.disabled = false;
  stopButton.disabled = true;
  
})

resumeButton.addEventListener('click', () => {
  timer = setInterval(slider, 3000);
  resumeButton.disabled = true;
  stopButton.disabled = false; 
})